/**
 * 
 */
package countrySQLParsing;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.BufferedWriter;

import java.io.IOException;

/**
 * @author aeris
 *
 */
public class ReaderWriter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String line;
		
		try {
			
			File countries = new File("countries.txt");
			File parsedCountries = new File("parsedCountries.sql");
			
			if (!countries.exists()) {
				countries.createNewFile();
			}
			
			if (!parsedCountries.exists()) {
				parsedCountries.createNewFile();
			}
			
			FileReader fr = new FileReader(countries);
			BufferedReader br = new BufferedReader(fr);
			
			FileWriter fw = new FileWriter(parsedCountries, false);
			BufferedWriter bw = new BufferedWriter(fw);
			
			line = br.readLine();
			bw.write("INSERT INTO easyjet_Country(CountryName)");
			bw.write("\nVALUES");
			
			while(line != null) {
				bw.write("\n\t('"+ line + "'" + "),");
				System.out.println(line);
				line = br.readLine();
			}
			
			fr.close();
			br.close();
			bw.close();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
